#-------------------------------------------------------------------------------
# Name:        РјРѕРґСѓР»СЊ1
# Purpose:
#
# Author:      user
#
# Created:     04.12.2018
# Copyright:   (c) user 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    temp = input()
    degree_type = temp[-1]
    degree_value = int(temp[0:-1])
    if degree_type == 'C' or degree_type == 'c':
        degree_value = round(degree_value * (9/5) + 32)
        print(str(degree_value))
    elif degree_type == 'F' or degree_type == 'f':
        degree_value = round ((degree_value - 32) * 5/9)
        print(str(degree_value))

if __name__ == '__main__':
    main()
